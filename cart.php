<?php
	
	$page_path = 'views/pages/cart.php';

	/* is login status boolean */
	$user_is_logged = true;

	/* active page, for marking it on the navbar */
	/* one of these: products, about, popular, offers, mixology, contact */
	/* if its none of those, just leave it blank */
	$active_nav = '';

	/* get items for cart */
	$cart_data = file_get_contents('./data/cart.json');
	$cart_data = json_decode($cart_data);
	$cart_data = $cart_data->products;

	/* calculate total price */
	$cart_total = 0;
	$cart_total = array_map(function($cart_item){
		return $cart_item->total;
	}, $cart_data);

	$cart_total = array_sum($cart_total);


	include 'views/partials/header.php';

	include 'views/templates/generic_template.php';

	include 'views/partials/footer.php';