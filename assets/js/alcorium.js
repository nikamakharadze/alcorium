(function($){
	/* make bootstrap's carousel swipeable */
	$('.carousel').carousel({
	  swipe: 30 // percent-per-second, default is 50. Pass false to disable swipe
	});


	/* bind jquery datepicker to registration form input */
	$('#register-birth-date-input').datepicker({ dateFormat: 'yy-mm-dd' });

	
	/* general listener for submiting forms */
	/* (you gotta still have a <input type="submit"> in the form tho) */
	/* cuz .submit() doesnt fire off html's validation popups, for some reason */
	$('button[data-submit-form]').on('click', function(e){
		e.preventDefault();

		var form = $(this).data('submit-form');
		$('#'+ form + ' input[type="submit"]').click();
	});



	/* dinamically change content area height,
		so that footer is always at the bottom of the screen */
	
	function adjustContentWrapperHeight(e){
		var pageHeight = $(window).height();
		var headerHeight = $('#main-header').outerHeight();
		var footerHeight = $('#main-footer').outerHeight();
		var wrapper = $('#main-content-wrapper');
		var minHeight = pageHeight - headerHeight - footerHeight;

		wrapper.css('min-height', minHeight + 'px');
	}
	
	$(window).on('resize', adjustContentWrapperHeight);
	
	adjustContentWrapperHeight(false);



	/* price range */
	var priceRangeFilter = $('#price-range-filter');

	if( priceRangeFilter !== null && priceRangeFilter.length > 0 ){
		var priceMin = priceRangeFilter.data('price-range-min');
		var priceMax = priceRangeFilter.data('price-range-max');
		var priceRange = priceMax - priceMin;

		var priceRangeBtnMin = $('#price-range-filter-min');
		var priceRangeBtnMax = $('#price-range-filter-max');
		var priceRangeLine = $('#price-range-filter-line');
		var priceRangeBtn = false;

		priceRangeBtnMin.html('<span>'+priceMin+'</span>');
		priceRangeBtnMax.html('<span>'+priceMax+'</span>');


		function priceRangeBtnMove(e){
			var x  = e.originalEvent.touches !== undefined ? e.originalEvent.touches[0].pageX : e.originalEvent.pageX;
			
			x -= priceRangeFilter.offset().left - (priceRangeBtn.width() / 2);
			
			// is cursor out of bounds?
			x = x < 0 ? 0 : x;
			x = x > priceRangeFilter.width() ? priceRangeFilter.width() : x;

			var maxLeft = priceRangeBtnMax.css('left').replace(/[^-\d\.]/g, '') - priceRangeBtnMax.width();
			var minLeft = priceRangeBtnMin.css('left').replace(/[^-\d\.]/g, '') + priceRangeBtnMin.width();
			var priceRangeInput;

			
			if( priceRangeBtn.attr('id') == 'price-range-filter-min' ){
				x = x > maxLeft ? maxLeft : x;
				priceRangeInput = $('input[name="filter_price_start"]');
			}else{
				x = x < minLeft ? minLeft : x;
				priceRangeInput = $('input[name="filter_price_end"]');
			}

			priceRangeBtn.css('left', x + 'px'); // update circle position

			var newPriceValue = x / priceRangeFilter.width() * 100;
				newPriceValue = parseInt(newPriceValue);
				newPriceValue = (priceMax - priceMin) * newPriceValue / 100;
				newPriceValue = parseInt(priceMin + newPriceValue);

			// add red line between two range circles
			var rangeColoredLine = $('<span></span>');
				rangeColoredLine.css({
					'width' : (maxLeft - minLeft +16) + 'px',
					'left'  : minLeft + 'px'
				});

			priceRangeLine.html(rangeColoredLine);
			
			priceRangeBtn.html('<span>' + newPriceValue + '</span>'); // update price value above circle
			priceRangeInput.val(newPriceValue) // update input value
		}


		/* attach listeners*/
		priceRangeBtnMin.on('touchstart mousedown', function(){
			priceRangeBtn = priceRangeBtnMin;
			$(document).on('touchmove mousemove', priceRangeBtnMove);
		});
		priceRangeBtnMax.on('touchstart mousedown', function(){
			priceRangeBtn = priceRangeBtnMax;
			$(document).on('touchmove mousemove', priceRangeBtnMove);
		});
		$(document).on('touchend mouseup', function(){
			$(document).off('touchmove mousemove', priceRangeBtnMove);
			priceRangeBtn = false;
		});
	}
	/* price range end */





	/* cart */

	/* change quantity of an item in a cart */
	function cartItemQuantityUpdate(e){
		e.preventDefault();

		var quantity = $(this).parent().parent().find('.cart-table-quantity');
		var quantityNum = parseInt(quantity.text());
		var qty = 0;

		if( $(this).data('click') == 'cart-item-quantity-decrease' ){
			
			if( quantityNum == 1){
				cartItemRemove(e); // remove item if quantity is decreasing to 0
				return false;
			}

			qty = quantityNum - 1;
		}else if( $(this).data('click') == 'cart-item-quantity-increase' ){
			qty = quantityNum + 1;
		}

		cartItemTotalUpdate($(this), qty); // update total price
		cartSubTotalUpdate(); // update cart Subtotal
		quantity.text(qty); // update quantity
	}

	/* remove item from cart */
	function cartItemRemove(e){
		e.preventDefault();
		$(e.target).closest('.cart-table-row').remove();
		cartSubTotalUpdate(); // update cart Subtotal
		updateCartItemCount(); // update item count in header navbar
	}

	/* update cart item's total price */
	function cartItemTotalUpdate(button, quantity){
		var itemTotal = button.closest('.cart-table-row').find('.cart-table-item-total');
		var itemPrice = button.closest('.cart-table-row').find('.cart-table-item-price');
			itemPrice = parseFloat(itemPrice.text());

		var newTotalPrice = (itemPrice * 100 * quantity) / 100;
			newTotalPrice = newTotalPrice.toFixed(2);
		itemTotal.text(newTotalPrice);
	}

	/* update carts total price */
	function cartSubTotalUpdate(){
		var subtotal = $('.cart-table-subtotal-number');
		var subtotalNum = 0;

		$('#cart-table .cart-table-item-total').each(function(){
			subtotalNum += parseFloat($(this).text()) * 100;
		});
		
		subtotalNum = subtotalNum / 100;
		subtotalNum = subtotalNum.toFixed(2);
		subtotal.text(subtotalNum);
	}

	/* update item count in a header navigation */
	function updateCartItemCount(){
		var itemCount = $('#cart-table .cart-table-row').length;
		$('.cart-item-count').text(itemCount);
	}

	/* attach listeners */
	$('button[data-click="cart-item-quantity-decrease"]').on('click', cartItemQuantityUpdate);
	$('button[data-click="cart-item-quantity-increase"]').on('click', cartItemQuantityUpdate);
	$('button[data-click="cart-item-remove"]').on('click', cartItemRemove);


	/* cart end */



	/* product inner */
	function productInnerQuantityUpdate(e){
		e.preventDefault();

		var quantity = $(this).parent().parent().find('.product-description-block-quantity');
		var quantityNum = parseInt(quantity.text());
		var qty = 0;

		if( $(this).data('click') == 'product-description-quantity-decrease' ){
			if( quantityNum == 1) return false; // you cant go lower than 1

			qty = quantityNum - 1;
		}else if( $(this).data('click') == 'product-description-quantity-increase' ){
			qty = quantityNum + 1;
		}

		productInnerTotalUpdate($(this), qty); // update total price
		quantity.text(qty); // update quantity
	}

	function productInnerTotalUpdate(button, quantity){
		var itemTotal = button.closest('#product-description-block').find('.product-description-block-subtotal span');
		var itemPrice = button.closest('#product-description-block').find('.product-description-block-price');
			itemPrice = parseFloat(itemPrice.text());

		var newTotalPrice = (itemPrice * 100 * quantity) / 100;
			newTotalPrice = newTotalPrice.toFixed(2);
		itemTotal.text(newTotalPrice);
	}

	/* attach listeners */
	$('button[data-click="product-description-quantity-decrease"]').on('click', productInnerQuantityUpdate);
	$('button[data-click="product-description-quantity-increase"]').on('click', productInnerQuantityUpdate);


	/* profile */

	// show profile info inputs
	function profileFormEdit(e){
		e.preventDefault();

		var form = $(this).data('click').replace('edit-', '');
		$('#'+form+' .profile-info-form-text').addClass('hidden');
		$('#'+form+' .profile-info-form-input').removeClass('hidden');
	}

	// attach listeners to profile edit buttons
	$('button[data-click="edit-profile-info-form"]').on('click', profileFormEdit);
	$('button[data-click="edit-profile-card-form"]').on('click', profileFormEdit);



	function profileMessageRemove(e){
		$(this).parent().parent().remove();
	}

	$('button[data-click="profile-message-remove"]').on('click', profileMessageRemove);
})($);