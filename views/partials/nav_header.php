<div class="row">
	<div class="col-xs-12 col-no-padding">
		<!-- Header -->
		<header id="main-header">
			
			<nav class="navbar navbar-inverse">
			    <div class="container-fluid">
			        
			        <!-- Logo and toggle get grouped for better mobile display -->
			        <div class="navbar-header">
			            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#alc-navbar-collapse-1" aria-expanded="false">
			                <span class="sr-only">Toggle navigation</span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			            </button>
			            <a class="navbar-brand header-logo" href="./home.php"></a>
			        </div>



			        <div class="collapse navbar-collapse" id="alc-navbar-collapse-1">
			            <!-- Main nav links -->
			            <ul class="nav navbar-nav navbar-left">
			            	<?php
			            		// $active_nav is a variable to check what page is active
			            		// to mark in the navigation (just adds the class="active")
			            		$cls = ' class="active"'; 
			            	?>
			                <li<?= ($active_nav == "products") ? $cls : '' ?>><a href="./products.php">Products</a></li>
			                <li<?= ($active_nav == "about") ? $cls : '' ?>><a href="./about.php">About Alcorium</a></li>
			                <li<?= ($active_nav == "popular") ? $cls : '' ?>><a href="#">Most Popular</a></li>
			                <li<?= ($active_nav == "offers") ? $cls : '' ?>><a href="#">Special Offers</a></li>
			                <li<?= ($active_nav == "mixology") ? $cls : '' ?>><a href="./mixology.php">Mixology</a></li>
			                <li<?= ($active_nav == "contact") ? $cls : '' ?>><a href="./contact.php">Contact</a></li>
			            </ul>

			            <!-- Profile and Cart -->
			            <ul class="nav navbar-nav navbar-right">
			            	<!-- check user login state boolean here -->
			            	<?php if( isset($user_is_logged) && $user_is_logged ): ?>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
										<!-- User's full name -->
										<div class="nav-user-name-wrapper">
											<div class="nav-user-name">Nika</div>
											<div class="nav-user-surname">Makharadze</div>
										</div>
										<!-- user's avatar (just change the url) -->
										<div class="nav-user-avatar hidden-xs" style="background-image:url('./assets/img/avatar.jpg')"></div>
										<span class="caret hidden"></span>
									</a>
									<ul class="dropdown-menu text-center">
										<li class="user-points">345 Points <img src="./assets/img/star.png"></li>
										<li role="separator" class="divider"></li>
										<li><a href="./profile.php">Profile</a></li>
										<li><a href="./profile.php">Notifications</a></li>
										<li><a href="#">Logout</a></li>
									</ul>
								</li>
			            	<?php else: ?>
			            		<li><a href="#">Login</a></li>
			            		<li><a href="#">Signup</a></li>
			            	<?php endif; ?>
			                <li>
			                	<a href="./cart.php" class="cart-icon">
			                		<span class="glyphicon glyphicon-shopping-cart">
			                			<!-- number of items in the cart -->
			                			<div class="cart-item-count">3</div>
			                		</span>
			                		<div class="visible-xs-inline-block">Cart</div>
			                	</a>
			            	</li>
			            </ul>
			        </div>
			    </div>
			</nav>

		</header>
	</div>
</div>