<?php if( isset($carousel) && count($carousel) > 0 ): ?>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php $carousel_index = 0; ?>
        <?php foreach($carousel as $indicator): ?>
            <?php if($carousel_index === 0): ?>
                <li data-target="#carousel-example-generic" data-slide-to="<?= $carousel_index ?>" class="active"></li>
            <?php else: ?>
                <li data-target="#carousel-example-generic" data-slide-to="<?= $carousel_index ?>"></li>
            <?php endif; ?>
            <?php $carousel_index++; ?>
        <?php endforeach; ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php foreach($carousel as $slide): ?>
            <div class="item<?= ( $slide === $carousel[0] ) ? ' active' : ''; ?>">
            <div class="carousel-image" style="background-image:url(<?= $slide['img'] ?>)">
                    <div class="carousel-caption"><?= $slide['caption'] ?></div>    
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>