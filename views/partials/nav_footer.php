<!-- Footer -->
<div class="row">
	<div class="col-xs-12 col-no-padding">
		<footer id="main-footer">
			<div class="row">
				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
					
					<!-- Footer nav -->
					<ul class="footer-nav">
						<li>
							<!-- Language change -->
							<ul class="language-bar">
								<li><a href="#" class="georgian-text">ქარ</a></li>
								<li><a href="#" class="active-lang">ENG</a></li>
							</ul>
						</li>

						<li><a href="./products.php">Products</a></li>
						<li><a href="#">Gifts</a></li>
						<li><a href="#">Special offers</a></li>
						<li><a href="./about.php">About us</a></li>
						<li><a href="./contact.php">Contact</a></li>
					</ul>

				</div>
				

				<!-- This part isnt visible on mobile -->
				<div class="col-sm-4 col-md-4 col-lg-4 hidden-xs text-center">
					<ul class="footer-social-links">
						<li><a href="#"><img src="./assets/img/fb.png"></a></li>
						<li><a href="#"><img src="./assets/img/twitter.png"></a></li>
						<li><a href="#"><img src="./assets/img/instagram.png"></a></li>
					</ul>

					<label class="footer-subscribe-label">Subscribe for newsletters</label>
					<form method="POST" class="footer-subscribe-form">
						<input type="email" name="subscribe_email" placeholder="Your e-mail" class="footer-subscribe-input">
						<button class="footer-subscribe-btn"></button>
					</form>

					<p class="footer-copyright">Copyright &copy; 2017</p>
				</div>
				

				<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 text-right">
					<img src="./assets/img/footer_logo.png" class="footer-logo">

					<p class="footer-text">International Transactions</p>
					<img src="./assets/img/visa.png" class="right footer-card-icon">
					<img src="./assets/img/mastercard.png" class="right footer-card-icon">
					<p class="footer-text">
						<a href="#" class="privacy-policy-link">Privacy & policy</a>
					</p>
				</div>

				<!-- This part is ONLY visible on mobile (or small enough screen, I guess) -->
				<div class="col-xs-12 visible-xs-block text-center">
					
					<ul class="footer-social-links">
						<li><a href="#"><img src="./assets/img/fb.png"></a></li>
						<li><a href="#"><img src="./assets/img/twitter.png"></a></li>
						<li><a href="#"><img src="./assets/img/instagram.png"></a></li>
					</ul>

					<label class="footer-subscribe-label">Subscribe for newsletters</label>
					<form method="POST" class="footer-subscribe-form">
						<input type="email" name="subscribe_email" placeholder="Your e-mail" class="footer-subscribe-input">
						<button class="footer-subscribe-btn"></button>
					</form>

					<p class="footer-copyright">Copyright &copy; 2017</p>
				</div>


			</div>
		</footer>
	</div>
</div>