<?php if( isset($product) ): ?> 
	<div class="row">
		<div class="col-xs-12 col-md-10 col-md-offset-1">
			<section id="product-description-block">
				<span class="product-inner-block-mixology mixology-carrot">Goes best with carrot juice</span>
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="product-description-block-image" style="background-image:url(<?= $product->image ?>)"></div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-sm-8">
								<h3 class="product-description-block-title"><?= $product->title ?></h3>
								<small class="product-description-block-litre"><?= $product->litre ?>litre</small>
							</div>
							<div class="col-xs-12 col-sm-4">
								<h3 class="product-description-block-price"><?= number_format($product->price, 2, '.', '') ?>₾</h3>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 product-description-block-text">
								<p><?= $product->description ?></p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-sm-offset-6">
								<div class="row">
									<div class="col-xs-6 col-sm-6">
										<h5 class="product-description-block-litre-select-label">Litrage:</h5>
										<select class="product-description-block-litre-select">
											<option>0.5</option>
											<option>0.7</option>
											<option>1</option>
										</select>
									</div>
									<div class="col-xs-6 col-sm-6 text-right">
										<h5 class="product-description-block-subtotal">
											<span><?= number_format($product->price, 2, '.', '') ?></span>
											₾
										</h5>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<h5 class="product-description-block-quantity-label">Quantity</h5>
								<div class="row product-description-block-quantity-row">
									<div class="col-xs-3 col-xs-offset-1">
										<button data-click="product-description-quantity-decrease">-</button>
									</div>
									<div class="col-xs-4 product-description-block-quantity">1</div>
									<div class="col-xs-3">
										<button data-click="product-description-quantity-increase">+</button>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-md-6">
								<button class="product-description-block-cart-btn">Add to cart</button>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>

	<!-- Similar products -->
	<div class="row">
		<div class="col-xs-12 col-md-10 col-md-offset-1">
			<div class="row">
				<div class="col-xs-12">
					<h4>Similar items</h4>
				</div>
			</div>	
			<div class="row similar-item-block-row">
				<?php if( isset($similar_items) ) : ?>
					<?php foreach($similar_items as $similar): ?>
						<div class="col-xs-12 col-md-2">
							<a href="./product.php?id=<?= $similar->id ?>">
								<article class="similar-item-block">
									<div class="row">
										<div class="col-xs-6 col-sm-12">
											<figure	class="similar-item-block-image" style="background-image:url(<?= $similar->image ?>)"></figure>
										</div>
										<div class="col-xs-6 col-sm-12">
											<h4 class="similar-item-block-title"><?= $similar->title ?></h4>
											<h3 class="similar-item-block-price"><?= number_format($similar->price, 2, '.', '') ?>₾</h4>
										</div>
									</div>
								</article>
							</a>
						</div>
					<?php endforeach;?>
				<?php endif;?>
			</div>	
		</div>
	</div>
	<br><br>
<?php endif;?>