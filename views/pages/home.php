<div class="row">
	<div class="col-xs-12 col-no-padding">
		<section id="main-carousel">
			<?php include './views/partials/carousel.php'; ?>
		</section>

		<a class="home-page-drink-block col-xs-12 col-sm-4 col-md-4 col-lg-4" 
			style="background-image:url('./uploads/home/1.png')">
			<div class="home-page-drink-block-caption">Special offers</div>
		</a>
		<a class="home-page-drink-block col-xs-12 col-sm-4 col-md-4 col-lg-4" 
			style="background-image:url('./uploads/home/2.png')">
			<div class="home-page-drink-block-caption">Recomended drinks</div>
		</a>
		<a href="#" class="home-page-drink-block col-xs-12 col-sm-4 col-md-4 col-lg-4" 
			style="background-image:url('./uploads/home/3.jpg')">
			<div class="home-page-drink-block-caption">Most popular</div>
		</a>
	</div>
</div>