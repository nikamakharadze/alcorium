<div class="col-xs-12 col-sm-10 col-sm-offset-1">
	<?php if( isset($products) ):
		$i = 0;
		foreach( $products as $product ):
			if( $i % 3 == 0 ): ?>
				<div class="row">
			<?php endif; ?>
				<div class="col-xs-12 col-md-4">
					<article class="product-list-block">
						<?php if( $is_mixology ): ?>
							<div class="product-list-block-mixology">
								<span class="product-list-block-mixology-label">Goes best with<br>carrot juice</span>
								<span class="mixology-icon mixology-carrot"></span>
							</div>
						<?php endif; ?>
						<a href="./product.php?id=<?= $product->id ?>">
							<figure class="product-list-block-image" style="background-image:url(<?= $product->image ?>)"></figure>
						</a>
						<div class="row">
							<div class="col-xs-12 col-md-9">
								<a href="./product.php?id=<?= $product->id ?>">
									<h4 class="product-list-block-title"><?= $product->title ?></h4>
								</a>
								<small class="product-list-block-litre"><?= $product->litre ?> litres</small>
							</div>
							<div class="col-xs-12 col-md-3">
								<span class="product-list-block-price"><?= number_format($product->price, 2, '.', '') ?>₾</span>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<button class="product-list-block-cart-button">Add to cart</button>
							</div>
						</div>
					</article>
				</div>
				
			<?php $i++;
			if( $i % 3 == 0 || $product == end($products) ): ?>
				</div>
			<?php endif;
		endforeach;
	endif; ?>
</div>
