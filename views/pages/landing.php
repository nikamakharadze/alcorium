<section>
	<div class="logo"></div>

	<h1>Alcorium represents a network of alcoholic stores in different format</h1>
	<p>You have to be over 18 to enter this site. Please enter your date of birth</p>

</section>

<section>
	<form method="post" id="landing-date-form">
		<input type="number" name="day" placeholder="Day" min="1" max="31" required>
		<input type="number" name="month" placeholder="Moth" min="1" max="12" required>
		<input type="number" name="year" placeholder="Year" min="1900" max="2100" required>

		<input type="submit" value="submit" class="hidden">
	</form>

	<div class="landing-date-form-submit">
		<div class="dotted-line"></div>
		<button data-submit-form="landing-date-form"></button>
	</div>
</section>