<section id="register-form-header">
	<div class="logo"></div>
</section>


<section id="register-form">
	<span class="register-form-border-top"></span>
	<span class="register-form-border-bottom"></span>
	<span class="border-cut-out bco-left bco-top"></span>
	<span class="border-cut-out bco-right bco-top"></span>
	<span class="border-cut-out bco-left bco-bottom"></span>
	<span class="border-cut-out bco-right bco-bottom"></span>
	
	<span class="register-form-border-back">
		<span class="register-form-border-back-left"></span>
		<span class="register-form-border-back-right"></span>
	</span>

	<form method="post">
		<input type="email" name="login_email" placeholder="E-mail">
		<input type="password" name="login_pass" placeholder="Password">


		<input type="submit" value="login" class="hidden">
	</form>

</section>


<section id="register-form-footer">
	<button class="register-btn" data-submit-form="register-form">Login</button>
	Don't have an account? <a href="./register.php">Register</a>
</section>