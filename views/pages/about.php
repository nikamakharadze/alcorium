<div class="row">
	<div class="col-lg-4 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-7 col-sm-offset-1 col-xs-12">
		<section id="register-form">
			<span class="register-form-border-top"></span>
			<span class="register-form-border-bottom"></span>
			<span class="border-cut-out bco-left bco-top"></span>
			<span class="border-cut-out bco-right bco-top"></span>
			<span class="border-cut-out bco-left bco-bottom"></span>
			<span class="border-cut-out bco-right bco-bottom"></span>
			<p class="about-us-text"><?= nl2br($about_text) ?>
		</section>
	</div>
</div>


