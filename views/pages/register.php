<section id="register-form-header">
	<div class="logo"></div>
</section>


<section id="register-form">
	<span class="register-form-border-top"></span>
	<span class="register-form-border-bottom"></span>
	<span class="border-cut-out bco-left bco-top"></span>
	<span class="border-cut-out bco-right bco-top"></span>
	<span class="border-cut-out bco-left bco-bottom"></span>
	<span class="border-cut-out bco-right bco-bottom"></span>
	
	<span class="register-form-border-back">
		<span class="register-form-border-back-left"></span>
		<span class="register-form-border-back-right"></span>
	</span>

	<form method="post">
		<input type="text" name="name" placeholder="Name, Surname">
		<input type="email" name="email" placeholder="E-mail">
		<input type="password" name="reg_pass" placeholder="Password">
		<input type="password" name="reg_pass_conf" placeholder="Repeat Password">
		<input type="text" name="phone" placeholder="Phone number">
		<input type="text" name="phone" placeholder="Address">

		<!-- Date picker -->
		<input type="text" name="birth_date" id="register-birth-date-input" placeholder="Birthday date">

		<!-- Add profile photo -->
		<label class="profile-photo-input">
			Add profile photo
			<input type="file" name="profile_photo" class="hidden">
		</label>

		<input type="submit" value="submit" class="hidden">
	</form>

	<!-- terms of service -->
	<div class="register-terms">By registering you agree to our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a></div>
</section>


<section id="register-form-footer">
	<button class="register-btn" data-submit-form="register-form">Register</button>
	Already have an account? <a href="./login.php">Sign in</a>
</section>