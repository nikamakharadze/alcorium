<br>
<div class="row">
	<div class="col-xs-12 col-lg-10 col-lg-offset-1">
		<div class="row">
			
			<!-- Profile info -->
			<div class="col-xs-12 col-sm-12 col-md-4 profile-info-col">
				<section id="profile-info-block">
					<div class="profile-info-block-avatar" style="background-image:url(<?= $profile->avatar ?>)"></div>

					<!-- User details -->
					<div class="row">
						<div class="col-xs-12">
							<form id="profile-info-form" method="post">
								<div class="row">
									<div class="col-xs-5 profile-info-form-text-label">Name:</div>
									<div class="col-xs-7 profile-info-form-text"><?= $profile->name ?></div>
									<div class="col-xs-7 profile-info-form-input hidden">
										<input class="form-control" name="name" value="<?= $profile->name ?>">
									</div>
								</div>

								<div class="row">
									<div class="col-xs-5 profile-info-form-text-label">Last name:</div>
									<div class="col-xs-7 profile-info-form-text"><?= $profile->last_name ?></div>
									<div class="col-xs-7 profile-info-form-input hidden">
										<input class="form-control" name="last_name" value="<?= $profile->last_name ?>">
									</div>
								</div>

								<div class="row">
									<div class="col-xs-5 profile-info-form-text-label">Password:</div>
									<div class="col-xs-7 profile-info-form-text"><?= $profile->password ?></div>
									<div class="col-xs-7 profile-info-form-input hidden">
										<input class="form-control" name="password" value="<?= $profile->password ?>">
									</div>
								</div>

								<div class="row">
									<div class="col-xs-5 profile-info-form-text-label">Phone:</div>
									<div class="col-xs-7 profile-info-form-text"><?= $profile->phone ?></div>
									<div class="col-xs-7 profile-info-form-input hidden">
										<input class="form-control" name="phone" value="<?= $profile->phone ?>">
									</div>
								</div>

								<div class="row">
									<div class="col-xs-5 profile-info-form-text-label">Address:</div>
									<div class="col-xs-7 profile-info-form-text"><?= $profile->address ?></div>
									<div class="col-xs-7 profile-info-form-input hidden">
										<input class="form-control" name="address" value="<?= $profile->address ?>">
									</div>
								</div>

								<div class="row">
									<div class="col-xs-5 profile-info-form-text-label">Birth date:</div>
									<div class="col-xs-7 profile-info-form-text"><?= $profile->birth_date ?></div>
									<div class="col-xs-7 profile-info-form-input hidden">
										<input class="form-control" name="birth_date" value="<?= $profile->birth_date ?>">
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<button class="profile-info-block-edit-button" data-click="edit-profile-info-form">Edit</button>
									</div>
								</div>
							</form>
						</div>
					</div>

					<!-- Card info -->
					<div class="row">
						<div class="col-xs-12">
							<h3 class="profile-info-block-payment-title">Payment info</h3>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<form method="post" id="profile-card-form">
								<div class="row">
									<div class="col-xs-5 profile-info-form-text-label">Card number:</div>
									<div class="col-xs-7 profile-info-form-text"><?= $profile->card_number ?></div>
									<div class="col-xs-7 profile-info-form-input hidden">
										<input class="form-control" name="card_number" value="<?= $profile->card_number ?>">
									</div>
								</div>
								<div class="row">
									<div class="col-xs-5 profile-info-form-text-label">Name on card:</div>
									<div class="col-xs-7 profile-info-form-text"><?= $profile->card_name ?></div>
									<div class="col-xs-7 profile-info-form-input hidden">
										<input class="form-control" name="card_name" value="<?= $profile->card_name ?>">
									</div>
								</div>
								<div class="row">
									<div class="col-xs-5 profile-info-form-text-label">Expiration date:</div>
									<div class="col-xs-7 profile-info-form-text"><?= $profile->card_expire ?></div>
									<div class="col-xs-7 profile-info-form-input hidden">
										<input class="form-control" name="card_expire" value="<?= $profile->card_expire ?>">
									</div>
								</div>
								<div class="row">
									<div class="col-xs-5 profile-info-form-text-label">CVC:</div>
									<div class="col-xs-7 profile-info-form-text"><?= $profile->card_cvc ?></div>
									<div class="col-xs-7 profile-info-form-input hidden">
										<input class="form-control" name="card_cvc" value="<?= $profile->card_cvc ?>">
									</div>
								</div>
								<button class="profile-info-block-edit-button" data-click="edit-profile-card-form">Edit</button>
							</form>
						</div>
					</div>



				</section>
			</div>
			
			<!-- Messages -->
			<div class="col-xs-12 col-sm-12 col-md-4 profile-messages-col">
				<h4 class="text-center">Notifications</h4>	
				<section id="profile-messages-block">
					<?php if( isset($messages) ): ?>
						<?php foreach($messages as $msg): ?>
							<?php $unread_class = $msg->read == 0 ? 'profile-messages-block-row-unread' : ''; ?>
							<div class="row profile-messages-block-row <?= $unread_class ?>">
								<div class="col-xs-12">
									<button class="profile-messages-block-remove" data-click="profile-message-remove">&times;</button>
									<h4 class="profile-messages-block-title"><?= $msg->title ?></h4>
									<p class="profile-messages-block-text"><?= $msg->text ?></p>
								</div>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</section>
			</div>


			<!-- Recent Orders -->
			<div class="col-xs-12 col-sm-12 col-md-4 profile-orders-col">
				<h4 class="text-center">Recent orders</h4>	
				<section id="profile-orders-block" class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th></th>
								<th>Product</th>
								<th>Order date</th>
								<th colspan="2">Shipping date</th>
							</tr>
						</thead>
						<tbody>
							<?php if( isset($orders) ): ?>
								<?php foreach($orders as $order): ?>
									<tr>
										<td>
											<a href="./product.php?id=<?= $order->id ?>">
												<img src="<?= $order->image ?>">
											</a>
										</td>
										<td>
											<a href="./product.php?id=<?= $order->id ?>">
												<?= $order->title ?>
											</a>
											<br><small><?= $order->uid ?></small>
										</td>
										<td><?= $order->order_date ?></td>
										<td><?= $order->shipping_date ?></td>
										<td><a href="#">View details</a></td>
									</tr>
								<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</section>
			</div>
		</div>
	</div>
</div>