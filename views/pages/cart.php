<div class="row">
	<div class="col-xs-12 col-md-10 col-md-offset-1">
		<section id="cart-table">
			<div class="row hidden-xs cart-table-head-row">
				<div class="col-sm-2 col-lg-1"></div>
				<div class="col-sm-10 col-lg-11">
					<div class="row">
						<div class="col-sm-5 col-lg-5">Product</div>
						<div class="col-sm-3 col-lg-2 text-center">Quantity</div>
						<div class="col-sm-2 col-lg-2 text-center">Price</div>
						<div class="col-sm-2 col-lg-2 text-center">Total</div>	
					</div>
				</div>
			</div>
			<?php if( isset($cart_data) ): foreach($cart_data as $cart_item): ?>
				<div class="row cart-table-row">
					<div class="col-xs-4 col-sm-2 col-lg-1">
						<div class="cart-product-image" style="background-image:url(<?= $cart_item->image ?>)"></div>
					</div>
					<div class="col-xs-8 col-sm-10">
						<div class="row">
							<div class="col-xs-12 col-sm-5">
								<div class="row">
									<button class="cart-table-remove-product-btn visible-xs-block" data-click="cart-item-remove">&times;</button> <!-- MOBILE "REMOVE" BUTTON -->
									<div class="col-xs-12 col-sm-12"><h4 class="cart-table-title"><?= $cart_item->title ?></h4></div>
									<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4"><small class="cart-table-litre"><?= $cart_item->litre ?> litres</small></div>
									<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8">
										<button class="cart-table-remove-product-btn hidden-xs" data-click="cart-item-remove">Remove</button> <!-- DESKTOP "REMOVE BUTTON" -->
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-3">
								<div class="row cart-table-quantity-row">
									<div class="col-xs-3 col-sm-3 col-sm-offset-2">
										<button data-click="cart-item-quantity-decrease">-</button>
									</div>
									<div class="col-xs-2 col-sm-2 cart-table-quantity"><?= $cart_item->quantity ?></div>
									<div class="col-xs-3 col-sm-3">
										<button data-click="cart-item-quantity-increase">+</button>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-2">
								<div class="row cart-table-price-row">
									<div class="col-xs-3 visible-xs-block">
										<span class="cart-table-mobile-label">Price: </span>
									</div>
									<div class="col-xs-9 col-sm-12">
										<span class="cart-table-item-price">
											<?= number_format($cart_item->price, 2, '.', '') ?>
										</span>
										₾
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-2">
								<div class="row cart-table-total-row">
									<div class="col-xs-3 visible-xs-block">
										<span class="cart-table-mobile-label">Total: </span>
									</div>
									<div class="col-xs-9 col-sm-12">
										<span class="cart-table-item-total">
											<?= number_format($cart_item->total, 2, '.', '') ?>
										</span>
										₾
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; endif; ?>
			<div class="row">
				<div class="col-xs-6 col-sm-12 cart-table-subtotal"><b>Subtotal</b></div>
				<div class="col-xs-6 col-sm-12 text-right"><b>
					<span class="cart-table-subtotal-number"><?= number_format($cart_total, 2, '.', '') ?></span>
					₾
				</b></div>
			</div>
		</section>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-md-10 col-md-offset-1">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<a href="./products.php" class="continue-shopping-link">&larr; Continue Shopping</a>
			</div>
			<div class="col-xs-12 col-md-6 text-right">
				<button class="checkout-btn">Proceed to Checkout</button>
			</div>
		</div>
	</diV>
</div>