<div class="row">
	<div class="col-lg-4 col-lg-offset-7 col-md-5 col-md-offset-6 col-sm-7 col-sm-offset-4 col-xs-12">
		<section id="register-form" class="contact-page-form">
			<span class="register-form-border-top"></span>
			<span class="register-form-border-bottom"></span>
			<span class="border-cut-out bco-left bco-top"></span>
			<span class="border-cut-out bco-right bco-top"></span>
			<span class="border-cut-out bco-left bco-bottom"></span>
			<span class="border-cut-out bco-right bco-bottom"></span>
			
			<form method="post">
				<input type="text" name="contact_name" placeholder="Name">
				<input type="email" name="contact_email" placeholder="E-mail">
				<textarea name="contact_text" placeholder="Message"></textarea>

				<input type="submit" value="Send" class="hidden">
			</form>
			<button class="register-btn" data-submit-form="register-form">Send</button>
		</section>

	</div>
</div>


