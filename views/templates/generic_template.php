<div class="container-fluid light-container">
	
	<?php include 'views/partials/nav_header.php'; ?>

	<div class="row">
		<div class="col-xs-12">
			<section id="main-content-wrapper">
				<?php include $page_path; ?>
			</section>
		</div>
	</div>

	<?php include 'views/partials/nav_footer.php'; ?>

</div>