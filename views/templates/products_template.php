<div class="container-fluid light-container">
	
	<?php include 'views/partials/nav_header.php'; ?>

	<div class="row">
		<div class="col-xs-12">
			<section id="main-content-wrapper">
				<!-- Filters -->
				<div class="row">
					<div class="col-xs-12 col-md-10 col-md-offset-1">
						<div class="row">
							<div class="col-xs-12 col-md-3">
								<span class="product-filter-label">Filter by:</span>
								<select id="product_filter_brand" class="product-filter-select">
									<option selected disabled>Brand</option>
									<option value="brand-1">Brand 1</option>
									<option value="brand-2">Brand 2</option>
									<option value="brand-3">Brand 3</option>
								</select>

								<select id="prodcut_filter_litre" class="product-filter-select">
									<option selected disabled>Litre</option>
									<option value="450">450ml</option>
									<option value="750">750ml</option>
									<option value="1000">1L</option>
								</select>
							</div>
							<div class="col-xs-12 col-md-9">
								<div class="row">
									<div class="col-xs-4 col-md-2 text-right">
										<span class="price-range-label">Price range</span>
									</div>
									<div class="col-xs-8 col-md-10">
										<section id="price-range-filter" data-price-range-min="<?= $filter_range_min ?>" data-price-range-max="<?= $filter_range_max ?>">
											<div id="price-range-filter-min"></div>
											<div id="price-range-filter-max"></div>
											<div id="price-range-filter-line"><span></span></div>
											
											<input type="hidden" name="filter_price_start" value="<?= $filter_range_min ?>">
											<input type="hidden" name="filter_price_end" value="<?= $filter_range_max ?>">
										</section>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- product blocks -->
				<div class="row">
					<?php include 'views/pages/products.php'; ?>
				</div>

				<!-- pagination -->
				<div class="row">
					<?php include 'views/partials/pagination.php'; ?>
				</div>
			</section>
		</div>
	</div>

	<?php include 'views/partials/nav_footer.php'; ?>

</div>