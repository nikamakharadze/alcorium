<div class="container-fluid" id="landing-page-container">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
			<?php include $page_path; ?>
		</div>
	</div>
</div>