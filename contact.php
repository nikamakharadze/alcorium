<?php
	
	$page_path = 'views/pages/contact.php';
	
	/* is login status boolean */
	$user_is_logged = true;

	/* active page, for marking it on the navbar */
	/* one of these: products, about, popular, offers, mixology, contact */
	/* if its none of those, just leave it blank */
	$active_nav = 'contact';

	include 'views/partials/header.php';

	include 'views/templates/about_template.php';

	include 'views/partials/footer.php';