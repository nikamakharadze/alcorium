<?php
	
	$page_path = 'views/pages/product.php';

	/* is login status boolean */
	$user_is_logged = true;

	/* active page, for marking it on the navbar */
	/* one of these: products, about, popular, offers, mixology, contact */
	/* if its none of those, just leave it blank */
	$active_nav = 'products';

	/* get product data */
	if( !isset($_GET['id']) ) exit;

	$products = file_get_contents('./data/products.json');
	$products = json_decode($products);
	$product = null;
	foreach($products->products as $prod){
		if( $_GET['id'] == $prod->id ){
			$product = $prod;
			break;
		}
	}

	$similar_items = array_slice(array_reverse($products->products), 0, 4);


	include 'views/partials/header.php';

	include 'views/templates/generic_template.php';

	include 'views/partials/footer.php';