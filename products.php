<?php
	
	$page_path = 'views/pages/products.php';

	/* set minimum and maximum values for the price range */
	$filter_range_min = 20;
	$filter_range_max = 500;
	
	/* is login status boolean */
	$user_is_logged = true;

	/* active page, for marking it on the navbar */
	/* one of these: products, about, popular, offers, mixology, contact */
	/* if its none of those, just leave it blank */
	$active_nav = 'products';

	/* get products */
	$products = file_get_contents('./data/products.json');
	$products = json_decode($products);
	$products = $products->products;

	// set boolean value for the /pages/products.php
	// if true - show the "goes best with" label
	$is_mixology = false;
	

	include 'views/partials/header.php';

	include 'views/templates/products_template.php';

	include 'views/partials/footer.php';