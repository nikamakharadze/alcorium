<?php
	
	$page_path = 'views/pages/home.php';
	
	/* is login status boolean */
	$user_is_logged = true;

	/* active page, for marking it on the navbar */
	/* one of these: products, about, popular, offers, mixology, contact */
	/* if its none of those, just leave it blank */
	$active_nav = '';

	/* array of images and captions for home page slider (carousel) */
	$dummy_caption = "Lorem ipsum is simply dummy text of the printing and typesetting industry. Lorem ipsum has been the industry";
	$carousel = array(
		array('img' => './uploads/slider/1.jpg', 'caption' => $dummy_caption),
		array('img' => './uploads/slider/2.jpg', 'caption' => $dummy_caption),
		array('img' => './uploads/slider/3.jpg', 'caption' => $dummy_caption),
		array('img' => './uploads/slider/4.jpg', 'caption' => $dummy_caption),
	);

	include 'views/partials/header.php';

	include 'views/templates/main_template.php';

	include 'views/partials/footer.php';