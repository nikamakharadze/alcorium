<?php
	
	$page_path = 'views/pages/about.php';
	
	/* is login status boolean */
	$user_is_logged = true;

	/* active page, for marking it on the navbar */
	/* one of these: products, about, popular, offers, mixology, contact */
	/* if its none of those, just leave it blank */
	$active_nav = 'about';

	/* about page text */
	$about_text = file_get_contents('./data/about.json');
	$about_text = json_decode($about_text);
	$about_text = $about_text->data;

	include 'views/partials/header.php';

	include 'views/templates/about_template.php';

	include 'views/partials/footer.php';