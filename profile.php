<?php
	
	$page_path = 'views/pages/profile.php';

	/* is login status boolean */
	$user_is_logged = true;

	/* active page, for marking it on the navbar */
	/* one of these: products, about, popular, offers, mixology, contact */
	/* if its none of those, just leave it blank */
	$active_nav = '';

	/* get profile info, messages and recent orders */
	$profile = file_get_contents('./data/profile.json');
	$profile = json_decode($profile);
	$profile = $profile->profile;


	$messages = file_get_contents('./data/messages.json');
	$messages = json_decode($messages);
	$messages = $messages->messages;

	$orders = file_get_contents('./data/orders.json');
	$orders = json_decode($orders);
	$orders = $orders->orders;

	include 'views/partials/header.php';

	include 'views/templates/generic_template.php';

	include 'views/partials/footer.php';